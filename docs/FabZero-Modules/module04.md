# 4. Découpe assistée par ordinateur

## Découpeuse

Le Fablab dispose de plusieurs machines de découpe (découpeuse laser et une découpeuse vinyle). La découpe laser est un procédé de fabrication qui consiste à découper la matière à partir d'une grande quantité d’énergie générée par un laser et concentrée sur une faible surface.

## Calibration

Avant toute découpe d'une pièce, il faut connaitre les propriétes du matériau afin de determiner quel type d'appareil de découpe le materiau pourra supporter, en effet certain matériaux tel que le vinyle ne peuvent supporter une découpe par laser par exemple. La connaissance du matériau nous permet également de trouver les paramètres adaptés à la découpe dont quelques unes sont la vitesse de passage et l'intensité. Il est courant de réaliser des plaques de calibration, sur du bois, carton ou tout autre matériau tel que sur l'image ci-dessous.

![](../images/Calibration.PNG)

Ces images de calibrations sont réalisés à partir de dessins sur Inskape. (Inkscape est un logiciel libre de dessin vectoriel).

## Kirigami et découpeuse laser

Après avoir paramétré la découpeuse nous allons découper quelques Kirigami. ci-dessous quelques images.

Une image d'un Kirigami réalisé sur Inskape

![](../images/kirigami_dessin.PNG)

Découpe de deux Kirigami

![](../images/kirigami1.jpg)

![](../images/kirigami2.jpg)

## Découpeuse Vinyle

Après l'utilisation de la découpeuse laser, place à la découpeuse vinyle. Je me suis fixé comme objectif de réaliser un auto-collant en forme tortue, pour y parvenir, il faut un fichier en dessin vectoriel, qu’on peut réaliser sur Inkscape.

![](../images/vynile.jpg)

![](../images/tortue.jpg)

Après avoir réalisé l'auto-collant, je l'ai collé sur mon pc.

## Conclusion

Cette séance fut instructive, nous pouvons dès à présent réaliser des auto-collants, découper des pièces dans le but d'assembler des objets. 7
lll
