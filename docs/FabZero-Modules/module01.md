# 1. Documentation: Installation

Cette semaine, nous avons commencé par explorer Gitlab, explorer ses différentes fonctionnalités. L’objectif est d’avoir une version locale, la modifier, puis envoyer la version finale sur le serveur.

## CONFIGURATION GITLAB

Concenrant l'Installation de Git, les différentes étapes sont très bien expliquées sur le site de [Clementine Benyakhou](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/clementine.benyakhou/modules/module01/#configuration-gitlab). J'ai suivi les différentes étapes pour installer et configurer Git.
J'ai eu quelques difficultés avec la configuration de la clé SSH, une fois resolu, la suite a été assez facile.

## RETOUR D'EXPERIENCE

Git est un outil très puissance qui offre différents fonctionnalités dont on peut citer de manière non exhaustif la possibilité de travailler à plusieurs chacun séparement.

## AVANTAGES ET INCONVENIENTS

- Comme avantages, on peut citer la gestion des branches. On peut travailler sur plusieurs projets en parallèle sans conflit majeur. Nous pouvons utiliser cette fonctionnalité.

- Les algorithmes de fusion (merge) : quand un fichier a été modifié par plusieurs personnes en même temps, Git sait s’adapter et choisir un algorithme qui fusionne intelligemment les lignes du fichier qui ont été modifiées. Si par hasard 2 personnes ont modifié en même temps la même ligne (cas rare, mais qui peut arriver), il y aura un conflit et Git laisse des marques dans le fichier pour dire qui a modifié quoi, et vous invite à décider ce que vous gardez.

- Comme défaut on peut citer la complexité : ce n’est pas un outil à mettre entre les mains de n’importe qui, il faut une certaines aptitudes avec l'invite de commande sous windows ou alors le système linux.

## UTILISATION DE GIT

Pour établir une connexion sécurisée entre les fichiers locaux et gitlab, j’ai du créér une clef SSH. La procédure est detaillée sur la page suivante [Clef SSH](https://docs.gitlab.com/ee/ssh/).

### PRINCIPALES COMMANDES DE Git

$ git config --global user.name "Tangui Felix"
$ git config --global user.email tanguifelix@example.com

$ git clone https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics.git # crée une copie locale du repository

$ git status # donne des infos sur le dossier cloné et le repository source

$ git pull copie les changements fait sur la version originale sur le clone

$ git add <fichierModifé.ext> # ajoute le fichier modifier à la liste des fichiers qui seront envoyés lors du prochain commit

$ git add -A # Ajoute tous les fichiers modifiés à l'index

$ git commit -m "<message du comit>" # prepare les données à envoyer au repository cloné avec le message indiqué.

$ git push # envoie le commit au repository cloné

### AFFICHAGE LOCAL DU SITE

Les modifications apportées à notre repository sur gitlab sont mis à jour 2 fois par semaine, dès lors il est important de pouvoir avoir une version locale du site (dans le but de voir comment les informations seront presentées sur le site) en utilisant la copie locale du repository. Pour cela, on utilise mkdocs, la commande ci-dessous permet de prévisualiser le site.

$ mkdocs serve #permet d'afficher le site dans un navigateur

### TRAITEMENT D'IMAGES

Nous pouvons inclure des images et vidéos sur le site, mais nous sommes limités en taille (plusieurs raisons sont à l'origine, on peut citer entre autre le stockage limité sur les serveur et le choix de privilégier une approche frugal).

Pour réduire la taille des images j'utilise Gimp, c'est un logiciel Open Source très pratique qui permet de réduire la taille des images. Il existe d'autres logiciels avec lesquels on peut effectuer la même tache, on peut citer Photoshop (payant), Paint (gratuit), PhotoScape (gratuit), Pixlr (gratuit).
