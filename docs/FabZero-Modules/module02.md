# 2. Conception Assistée par Ordinateur

Deuxième semaine de cours: l'objectif de cette session portait principalement sur des outils de conception assistée par ordinateur. Ainsi nous nous sommes focalisés sur deux logiciels open source que sont FreeCAD et OpenSCAD.

## OBJECTIF DE CE MODULE

- Comprendre et choisir un outil pour dessiner un pièce en 3D
- Réaliser le tutoriel afin de créer un kit Lego.

## CHOIX DU LOGICIEL

Dans ce module nous devons Choisir un Flexilinks sur le site de BUY CMR et le dessiner sur un des programme de modélisation 3D (FreeCAD ou OpenSCAD).

Il y a 2 grandes familles de logiciels 3D, les logiciels graphiques et les logiciels de codage. Pour les logiciels de codage génèrent des formes à partir d'un ensemble de code écrit tandis que les logiciels graphiques génèrent des formes à partir d'un schéma 2D qui sera par la suite extrudé pour avoir la forme 3D. Pour le design de ma pièce, j'ai choisi d'utiliser FreeCAD.

## FREECAD

FreeCAD est un logiciel de CAO paramétrique. Pour réaliser une pièce, on partira généralement d'un modèle 2D sur lequel on donnera des contraintes et paramètres. Ces paramètres sont modifiables à tout moment et permettent de modifier la géometrie rapidement sans devoir refaire un nouveau model.

FreeCAD permet un plus grande liberté dans la création de modèle 3D comparativement à OpenSCAD.Il est à noter que il existe une intégration de OpenSCAD dans FreeCAD, c’est à dire qu’il est possible de réaliser une modélisation simple dans OpenSCAD et puis de venir le paufiner et complexifier dans FreeCAD.

## DESIGNER UN FLEXILINKS - TUTORIEL

Il existe une multitude de logiciels de modélisations disponibles, deux nous ont été présentés lors du cours : OpenSCAD et FreeCAD, deux logiciels qui diffèrent par leur approche. D’une part, OpenSCAD, un logiciel ou l’utilisateur décrit par des équations la géometrie à modéliser dans un langage de programmation. D'autre part, FreeCAD, un logiciel basé sur le dessin de la pièce (on commence par un schéma 2D qu'on va par la suite extrudé pour avoir la pièce en 3D). Ces deux logiciels sont complémentaires et presentent chacun ses avantages et ses inconvénients. Une pièce crée avec OpenSCAD peut être importée et modifiée dans FreeCAD.

## MODELISATION FREECAD

Le but de cette session est de choisir un Flexilinks sur le [site](https://www.compliantmechanisms.byu.edu/flexlinks) et de le modéliser sur l'un des logiciels cités plus haut. J'ai choisi de modéliser ce [modèle](https://www.thingiverse.com/thing:3016949). Pour ce faire j'ai utilisé FreeCAD, les différents étapes séront présentées ci-dessous.

- La première étape consiste à choisir un plan de travail qui dans mon cas est le plan XY puis representer un rectangle qui sera par la suite extrudé pour avoir un parallélépipède tel que tu représenté sur la photo ci-dessous.

![](../images/FreeCad1.PNG)

Sur cette image on voit le rectangle qui sera extrudé afin d'avoir un parallélépipède. Une fois le rectangle terminé, il faut extruder sur une hauteur suivant l'axe Z, c'est ainsi qu'on génère la géometrie représentée ci-dessous.

![](../images/FreeCad2.PNG)

- La seconde étape est celle qui mènera à la figure finale avec les 6 petits cylindre sur la face supérieur. Pour y parvenir, il faut de nouveau définir un plan de travail. La face supérieur sera notre nouveau plan de travail, sur celle ci, nous allons représenter un cercle qu'on va paramétrer puis par symétrie et translation on va construire les 6 cercles et pour finir nous allons extruder ces cercles sur une hauteur Z. Ci-dessous l'image associée à la description

![](../images/FreeCad3.PNG)

Cette dernière image constitue le Flexlinks à imprimer lors de la prochaine séance sur imprimante 3D.
