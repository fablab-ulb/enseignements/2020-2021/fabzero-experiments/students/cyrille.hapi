# 6. Electronique 2 - Fabrication

## Objectif de la semaine prochaine:

- Cloturer les issues pour la semaine prochaine afin de se concentrer sur le projet la semaine d'après.

- Réflechir sur une probélmatique (analyser la problématique, chercher les donées dessus) qui nous tient à coeur dans le but de choisir son projet.

## Introduction

La semaine dernière nous avons manipuler des cartes Arduino, cette semaine le but est de faire notre propre carte Arduino. Nous allons faire un design facile afin que la séance ne soit pas très difficile.

Contrairement à une Arduino qui comporte différents élements (tel que les microcontroleurs, les entrées/sorties, l'interface), sur cette carte plusieurs composants sont intégrés et par conséquent la complexité de la carte sera moins éléve que celle d'une Arduino.

## Présentation du PCB

controler le PCB afin de vérifier qu'il n'y a pas de defauts sur la carte, concretement il s'agit de vérifier que les pistes sont bien dégagées, qu'il n'y a pas de faux contacts entre pistes. Ci-dessous une image du PCB.

![](../images/PCB.jpg)

## Soudage des composants (puce)

souder dans le bon sens, sur la chip, il y a un point blanc qui symbolise la patte numéro 1, ce petit point devra être orienté vers le hat en gauche sur notre chip. Une autre méthode pour identifier le bon sens est que le texte apparaisse dans le bon sens quand on le connecteur USB est vers la droite. Le but du respect de cette disposition est d'éviter de griller le PCB Ci-dessous une image de la bonne disposition:

![](../images/disposition_ok.jpg)

- Pour souder, il faut mettre une petite goute sur le bout de notre fer puis venir mouiller la piste sur le PCB. Une bonne soudure doit etre lisse et brillante, si on n'a pas cet effet ce n'est pas grave car on pourra revenir dessus et modifier. La température du fer à souder est de 320 degré. ci-dessous une image du kit de soudage complet.

![](../images/outils.jpg)

### Soudage du regulateur

Une fois la puce soudé sur le PCB, on peut passer au soudage du regulateur, le regulateur est ce petit circuit imprimé qu'on peut voir sur la photo ci-dessous (circuit qui n'est pas soudé sur le PCB). Le rôle du régulateur est d'abaisser la tension de 5V à 3.3V afin d'éviter de griller la puce.

![](../images/regulateur.jpg)

### Soudade du condensateur

Le condensateur sert à stabiliser le regulateur. C'est la pièce la plus petite de ce circuit.

![](../images/condensateur.jpg)

Ici une image du circuit completement soudé.

![](../images/soudage_complet.jpg)

### Détection de la carte

Après avoir fini les soudages, la carte à été btanché sur l'entrée USB du pc afin de vérifiér que la soudure s'est bien passé et que la carte fonctionne. Le but est de vérifier qu'il est bien detecté par le système et identifié dans le gestionnaire de périphériques de Windows.
Notons que cette puce peut devenir un clavier, une sourie, on peut l'utiliser dans plusieurs manières différentes, on peut également charger un programme à l'intérieur. Ci-dessous une image du branchement sur le pc et une image montrant qu'elle est bien detectée par Windows.

![](../images/branchement_USB.jpg)

![](../images/port_COM5.PNG)

### Test de la carte (allumage d'une diode)

Après avoir vérifié le bon fonctionnement de la carte, il est temps d'utiliser cette carte, pour ce faire nous allons l'utiliser pour allumer/faire clignotter une diode (module précédent: projet 1). Dans ce montage nous avons utilisé des éléments montés en surface (microLED, resistance,...). Ci-dessous une image du circuit finalavec microLED à tester.  

![](../images/circuit_final.jpg)

Pour allumer la diode, il faut écrire le programme et charger le programme dans la carte. Nous allons utiliser le logiciel Arduino pour notre expérience. Ci-dessous une image de la microLED allumée.

![](../images/led_allumee)

## Conclusion

Cette séance fût l'une des plus instructives, nous avons appris à concevoir et fabriquer notre propre carte Arduino, pour cela nous avons dû apprendre la soudure, lire un circuit afin de bien brancher les composants. Pour finir, les circuit avec des composants montés en surface ne sont plus un mystère.
