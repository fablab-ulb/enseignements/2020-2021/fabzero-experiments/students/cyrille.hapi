# 5. Electronique 1 - Prototypage

## But de la session

Cette semaine nous avons commencé avec l'électronique. Le but est d'avoir une connaissance de base en électronique, de tester différents types de montages, de capteurs et comprendre le fonctionnement.

Lors de cette séance, nous allons utilsier un Arduino Uno. L’Arduino Uno est un microcontrôleur programmable qui permet de contrôler des éléments mécaniques : systèmes, lumières, moteurs, etc. Un microcontrôleur est un circuit intégré qui rassemble les éléments essentiels d'un ordinateur à savoir: le processeur, les mémoires (mémoire morte et mémoire vive), unités périphériques et interfaces d'entrées-sorties.


## Arduino Uno

Ci-dessous une photo de l'Arduino Uno.

![](../images/Arduino_Uno.PNG)

- Avantages de l'Arduino Arduino Uno

Il existe plusieurs modèles de cartes Arduino mais le modèle Uno est le plus répandu et permet beaucoup de possibilités.

L’Arduino Uno possèdent des caractéristiques suffisantes pour réaliser beaucoup de petits projets. Il possède : 14 entrées/sorties numériques, 6 entrées analogiques, une mémoire flash de 32 KB, un de SRAM 2 KB, un EPPROM de 1 KB.

L’un des avantages de ce microcontrôleur est sa facilité d’utilisation, il est vivement recommandé pour des débutants. La carte se connecte très facilement à un ordinateur via un câble USB fourni. Un autre avantage de l’Arduino Uno est sa facilité d’accès, il est disponible un peu partout sur internet à un prix abordable proche de 25 €.

- Les inconvénients de l'Arduino Uno

Pour utiliser l’Arduino Uno, il faudra passer par l'apprentissage d'un langage de la programmation (le langage C). Si l'on est débutant, quelques heures sur les guides et tutos explicatifs seront donc nécessaires pour arriver à comprendre comment le système fonctionne. Ayant déjà programmé en C++ par le passé, la prise en main a été assez facile dans mon cas. Comme dernier desavantages on peut noter qu'on a une mémoire embarquée limitée qui rend les programmes complexes difficiles à réaliser.

## Projet (feux de circulation)

Lors de cette session, nous avions le choix entre différents projets. Après avoir consulté l'ensemble des projets, j'ai choisi le projet 3 qui consiste à réaliser un systeme de feu de signalisation. Pour y parvenir, nous disposons de fils de cablages, de diodes led, de resistances, d'un interrupteur et de l'Arduino Uno. Avant de me lancer dans ce projet, j'ai voulu faire un test rapide d'allumage d'un diode led (projet 1).

L'image ci-dessous correspond au projet 1, on voit que la led est allumé. Le but de ce projet est surtout qu'on écrive le code permettant de faire fonctionner le circuit, qu'on le charge à travers le port USB. On nous fourni le montage (pour ceux n'ayant fait d'électricité ou électronique peuvent avoir des difficultés à reproduire ce montage sur l'Arduino).

![](../images/Projet1.jpg)

Après avoir réalisé le projet 1 avec succès, je suis passé au projet 3. Dans ce projet, le circuit est fourni mais pas le code qui permet de gérer les feux de circulation. Selon les ports d'entrées/sorties et mes cablages, je vais écrire le code correspondant et gérer surtout la séquence d'allumage des feux.

Ci-dessous une la photo du projet 3 à réaliser

![](../images/Projet3.jpg)

Ci-dessous le cablage correspondant à ce projet

![](../images/Projet3-cablage.jpg)

## Code

Pour faire fonctionner ce montage, il faut le brancher sur le port USB d'un pc et se connecter sur le logiciel qui permettra la communication avec l'arduino. Avant de brancher, il faut écrire le code qui sera par la suite chargée dans l'Arduino. Ci-dessous l'interface du logiciel avec quelques lignes de code

![](../images/Inter_arduino.PNG)

Ci-dessous le code qui gère les feux de circulation. La séquence est la suivante:

- Le feu vert est allumé pour les véhicules pendant que le rouge est allumé pour les piétons.

- Après avoir appuyé sur l'intérrupteur, le feu jaune s'allume pour les véhicules annoncant le passage au rouge pour les véhicules. Quand le feu rouge s'allume (véhicules), il faut un certains laps de temps avant l'allumage du feu vert pour les piétons (mesure de sécurité). Après ce laps de temps, pendant qu'on est au rouge pour les véhicules, c'est le vert pour les piétons.

- La fin du feu vert est annoncé par le clignotement ce celui-ci afin que les piétons qui sont encore sur la chaussée se dépêchent.A la fin du clignotement, le rouge s'allume pour les piétons pendant que le jaune pour la voiture annonce la transition vers le vert et retour à l'état initial.

Sous le code, vous verez quelques photos du test et plus bas un lien vers la vidéo du fonctionnement.

```
/*
   Feux de circulation
*/
int carRed = 12; //assignation lumière rouge véhicule
int carYellow = 11;
int carGreen = 10;
int button = 9; //button pin
int pedRed = 8; //assignation Lumière piétons
int pedGreen = 7;
int crossTime =5000; // Temps pour traverser la route
unsigned long changeTime;// Durée d'appui du boutton

void setup() {
    pinMode(carRed, OUTPUT);
    pinMode(carYellow, OUTPUT);
    pinMode(carGreen, OUTPUT);
    pinMode(pedRed, OUTPUT);
    pinMode(pedGreen, OUTPUT);
    pinMode(button, INPUT);
    digitalWrite(carGreen, HIGH); // Changement vers le vert
    digitalWrite(pedRed, HIGH);
}

void loop() {
  int state = digitalRead(button);
        // vérifie si le bouton est appuyé et si s'est écoulé plus de 5 secondes depuis le dernièr appuie sur le bouton.
        if(state == HIGH && (millis() - changeTime)> 5000){
               //Appelle la fonction pour changer la lumière
               changeLights();
        }
}

void changeLights() {
  digitalWrite(carGreen, LOW); //Vert étteint
  digitalWrite(carYellow, HIGH); // Jaune allumée
  delay(2000); //stop time 2 seconds

  digitalWrite(carYellow, LOW); //Jaune étteint
  digitalWrite(carRed, HIGH); //Rouge allumée
        delay(1000); //stop time de 1 seconde pour sécurité

  digitalWrite(pedRed, LOW); //diode rouge étteint
  digitalWrite(pedGreen, HIGH); //diode Jaune allumée

  delay(crossTime); //attente pendant une période prédéfinie

  //faire clignoter la diode en vert
        for (int x=0; x<10; x++) {
          digitalWrite(pedGreen, HIGH);
    delay(250);
    digitalWrite(pedGreen, LOW);
    delay(250);
         }

  digitalWrite(pedRed, HIGH);//diode rouge allumée
  delay(500);

  digitalWrite(carRed, LOW); //rouge étteint
  digitalWrite(carYellow, HIGH); //jaune allumée
  delay(1000);
  digitalWrite(carYellow, LOW); //jaune étteint
  digitalWrite(carGreen, HIGH);

  changeTime = millis(); //enregistrer le temps depuis le dernier changement de lumière puis retour à la boucle du programme principal
}
```



### Video

Pour voir la vidéo, vuillez cliquer [ici](https://youtu.be/xKLJIxNVT1U)

## Conclusion

Cette séance fut très instructive, bien que par le passé j'ai eu à utiliser un Arduino j'ai appris de nouvelles techniques pour resoudre un problème et surtout ce fut une excellente séance de révision.
