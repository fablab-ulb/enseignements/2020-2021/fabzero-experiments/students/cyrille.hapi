# 3. Impression 3D

Pour cette nouvelle séance, nous avons eu un long exposé sur le mode de fonctionnement d'une imprimante 3D, comment transformer un fichier issue d'une modélisation en fichier stl utilisable par les imprimantes 3D. Le FabLab est équipé en imprimantes Prusa I3MK3S, c’est donc le slicer Prusa qu’on utilisera.

L'interface du logiciel se présente comme suit:

![](../images/Prusa.PNG)

Avant de commencer l'impression, il faur régler les paramètres. Les valeurs ci-dessous sont celles qui ont été utilisé pour l'impression, comme valeurs on peut citer les principales à savoir:
- Le reglage d’impression
- Couche et perimetre
- Remplissage:

 ![](../images/Prusa-Parametre.PNG)

## Dessin à imprimer

Partager le fichier stl (le rendre téléchargeable pour nimporte quel utilisateur).
Ci-dessous le rendu avant impression, le but est de visualiser et vérifier si l'objet est bien centré, nous avons le rendu final. Pour finir, il faut exporter le G-code dans une carte SD mise a disposition et l'insérer dans l’imprimante puis lancer l’impression en tournant la molette a droite.

 ![](../images/Prusa-Imp.PNG)

 Ci-dessous une photo prise lors de l'impression par l'imprimante 3D

 ![](../images/Imp.jpg)

 Le résultat final ci-dessous

 ![](../images/para1.jpg)
