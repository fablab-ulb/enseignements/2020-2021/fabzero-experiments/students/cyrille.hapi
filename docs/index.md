## A PROPOS DE MOI

![](images/Cyrille.jpg)

Salut je m'appelle Hapi Cyrille, passionné par des projets ayant une dimmension sociale, des projets pouvant améliorer le quotidien des personnes défavorisées. Je travaille actuellement sur un projet de filet à nuages au FrugalLab. Le but du projet est la recolte d'eau à partir de filets. Pour y parvenir un filet sera dresser sur une montagne, ce filet sera traversé par des des nuages puis des particules d'eau seront captées et de l'eau liquide sera recoltée.

Ce projet est une réponse aux problème d'eau qu'on rencontre dans les pays en voie de developpement (Afrique, Amérique du sud et Asie). Cette dimension humaine a été le facteur le plus important dans mon choix. Ce filet sera fait à partir de matériaux disponible dans la nature et ayant un coût très bas ceci afin qu'il soit facilement reproductible un peu partout dans le monde. le but ici étant de répondre à un problème en apportant une solution ayant un coût financier et environnementale le plus bas possible.

Pour y parvenir différentes études seront menées dont les principales porteront sur le caractère hydrophobe ou hydrophile des matériaux constituant le filet, un accent sera mis sur la resistance des matériaux (rigidité, résilience) et pour finir d'autres aspects importants tels que l'aérodynamisme, les motifs en kirigami seront explorés.

Sur ce site, je partagerai avec vous l'évolution de ce projet, du début jusqu'à sa fin. Je vous souhaite une bienvenue et j'espère que ce sera un plaisir de découvrir ce projet.

## COMPETENCES

Pour réaliser ce projet multidiciplinaire, diverses compétences scientifiques et techniques seront nécessaires, à savoir l'utilisation des logiciels de design (dans ce cas spécifique il y a une multitude de logiciels mais pour garder un esprit Frugal nous utiliseront des logiciels open source tels que: FreeCAD, OpenSCAD), des découpeuse laser, imprimante 3D, ...  
